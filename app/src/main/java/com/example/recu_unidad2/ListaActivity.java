package com.example.recu_unidad2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Modelo.ImcsDb;

public class ListaActivity extends AppCompatActivity {

    private ListView listView;
    private AdapterImc adapter;
    private List<Imc> listaImcs;
    private ImcsDb imcsDb;
    private Button btnRegresar, btnBorrar;
    private TextView lblTotalImcsRes;
    private ArrayList<Imc> imcs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        imcsDb = new ImcsDb(this);

        // Obtener la referencia de la ListView
        listView = findViewById(R.id.lstRegistros);

        btnRegresar = findViewById(R.id.btnRegresar);
        btnBorrar = findViewById(R.id.btnBorrar);

        // Crear la lista de ventas
        imcs = imcsDb.allImcs();
        adapter = new AdapterImc(this, imcs);
        listView.setAdapter(adapter);

        lblTotalImcsRes = findViewById(R.id.lblTotalIMCRes);

        calcularTotal();

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Regresar a la pantalla anterior
                Intent intent = new Intent(ListaActivity.this, MainActivity.class);
                startActivity(intent);
                finish(); // Cierra la actividad actual (MainActivity)
            }
        });

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                borrarHistorial();
            }
        });

    }

    private void calcularTotal() {
        float total = 0f;
        float promedio = 0f;
        listaImcs = imcsDb.allImcs();

        for (Imc imc : listaImcs) {
            total += (imc.getPeso()/(imc.getAltura() * imc.getAltura()));
            promedio = total / imcs.size();
        }
        lblTotalImcsRes.setText(String.valueOf(promedio));
    }

    private void borrarHistorial(){
        // Crear una instancia de ImcDb para acceder a la base de datos
        imcsDb = new ImcsDb(ListaActivity.this);

        // Llamar al método deleteAllImcs() para borrar todos los registros
        imcsDb.deleteAllImcs();

        adapter.notifyDataSetChanged();
        // Mostrar un mensaje indicando que el historial ha sido borrado
        Toast.makeText(ListaActivity.this, "Historial borrado", Toast.LENGTH_SHORT).show();

    }

}