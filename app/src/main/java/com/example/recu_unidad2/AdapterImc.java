package com.example.recu_unidad2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class AdapterImc extends BaseAdapter {

    private List<Imc> datos;
    private Context context;

    public AdapterImc(Context context, List<Imc> datos) {
        this.context = context;
        this.datos = datos;
    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public Object getItem(int position) {
        return datos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            // Inflar el diseño personalizado del elemento de la lista
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.imcs_item, parent, false);

            // Crear un ViewHolder para mantener las referencias a las vistas
            viewHolder = new ViewHolder();
            viewHolder.lblId = convertView.findViewById(R.id.lblId);
            viewHolder.lblAltura = convertView.findViewById(R.id.lblAltura);
            viewHolder.lblPeso = convertView.findViewById(R.id.lblPeso);
            viewHolder.lblImc = convertView.findViewById(R.id.lblIMC);

            // Establecer el ViewHolder como una etiqueta de la vista convertida
            convertView.setTag(viewHolder);
        } else {
            // Si convertView no es nulo, obtener el ViewHolder de la etiqueta
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Obtener el elemento de datos para la posición actual
        Imc imc = datos.get(position);

        // Establecer los datos en las vistas del diseño personalizado
        viewHolder.lblId.setText(String.valueOf(position+1));
        viewHolder.lblAltura.setText(String.valueOf(imc.getAltura()));
        viewHolder.lblPeso.setText(String.valueOf(imc.getPeso()));
        viewHolder.lblImc.setText(String.valueOf(imc.getImc()));

        // Obtener los valores de cantidad y precio
        float altura = imc.getAltura();
        float peso = imc.getPeso();

        float ResImc = (peso / (altura * altura));

        String totalString = String.format("%.2f", ResImc);
        viewHolder.lblImc.setText(totalString);

        return convertView;
    }

    private static class ViewHolder {
        TextView lblId;
        TextView lblAltura;
        TextView lblPeso;
        TextView lblImc;
    }

}
