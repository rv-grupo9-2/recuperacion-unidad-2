package com.example.recu_unidad2;

import java.io.Serializable;
import java.util.ArrayList;

public class Imc implements Serializable {

    private int id;
    private float altura;
    private float peso;
    private float imc;

    public Imc () {

    }

    public Imc (float altura, float peso, float imc) {
        this.altura = altura;
        this.peso = peso;
        this.imc = imc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public Float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public Float getImc() {
        return imc;
    }

    public void setImc(float imc) {
        this.imc = imc;
    }

    public static ArrayList<Imc> llenarImcs(){
        ArrayList<Imc> imcs=new ArrayList<>();

        imcs.add(new Imc(1.70f,55.00f,19.00f));

        return imcs;
    }

}
