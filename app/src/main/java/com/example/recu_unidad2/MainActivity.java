package com.example.recu_unidad2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import Modelo.ImcsDb;

public class MainActivity extends AppCompatActivity {

    private EditText txtAltura, txtPeso, txtIMC;
    private Button btnCalcular, btnGuardar, btnVerHistorial, btnSalir;
    private Imc imc;
    private boolean imcCreado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicializar los elementos de la interfaz
        txtAltura = findViewById(R.id.txtAltura);
        txtPeso = findViewById(R.id.txtPeso);
        txtIMC = findViewById(R.id.txtIMC);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnVerHistorial = findViewById(R.id.btnVerHistorial);
        btnSalir = findViewById(R.id.btnSalir);


        // Agregar listeners de eventos a los botones
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String altura = txtAltura.getText().toString().trim();
                String peso = txtPeso.getText().toString().trim();

                float alturaFloat;
                float pesoFloat;
                float imcPersonal= 0.0f;

                try {
                    alturaFloat = Float.parseFloat(altura);
                    pesoFloat = Float.parseFloat(peso);
                } catch (NumberFormatException e) {
                    Toast.makeText(MainActivity.this, "Error en el formato de los valores numéricos", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (altura.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingrese la altura", Toast.LENGTH_SHORT).show();
                } else if (peso.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingrese el peso", Toast.LENGTH_SHORT).show();
                } else {


                        imcPersonal = (pesoFloat / (alturaFloat * alturaFloat));
                        txtIMC.setText(String.valueOf(imcPersonal));
                        imcCreado = true;
                }

                imc = new Imc(alturaFloat, pesoFloat, imcPersonal);
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String altura = txtAltura.getText().toString().trim();
                String peso = txtPeso.getText().toString().trim();

                if (altura.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingrese la altura", Toast.LENGTH_SHORT).show();
                    return;
                } else if (peso.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingrese el peso", Toast.LENGTH_SHORT).show();
                    return;
                }

                float alturaDb = imc.getAltura();
                float pesoDb = imc.getPeso();
                float imcDb = imc.getImc();


                if (imcCreado) {
                    // Crear una instancia de Venta con los valores obtenidos
                    Imc imc = new Imc(alturaDb, pesoDb, imcDb);

                    // Crear una instancia de VentasDb
                    ImcsDb imcsDb = new ImcsDb(MainActivity.this);

                    // Insertar la venta en la base de datos
                    long imcId = imcsDb.insertImc(imc);

                    if (imcId != -1) {
                        Toast.makeText(MainActivity.this, "Imc registrado correctamente", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Error al registrar el imc", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "No se ha realizado un registro", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnVerHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListaActivity.class);
                startActivity(intent);
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
            }
        });

    }


}