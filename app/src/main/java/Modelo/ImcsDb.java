package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.recu_unidad2.Imc;

import java.util.ArrayList;

public class ImcsDb implements Persistencia, Proyeccion {

    private Context context;
    private ImcsDbHelper helper;
    private SQLiteDatabase db;

    public ImcsDb(Context context, ImcsDbHelper helper){
        this.context=context;
        this.helper=helper;
    }

    public ImcsDb(Context context){
        this.context=context;
        this.helper=new ImcsDbHelper(this.context);
    }


    @Override
    public void openDataBase() {
        db=helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    public long insertImc(Imc imc) {

        ContentValues values=new ContentValues();

        values.put(DefineTable.HistImc.COLUMN_NAME_ALTURA, imc.getAltura());
        values.put(DefineTable.HistImc.COLUMN_NAME_PESO, imc.getPeso());
        values.put(DefineTable.HistImc.COLUMN_NAME_IMC, imc.getImc());

        this.openDataBase();
        long num = db.insert(DefineTable.HistImc.TABLE_NAME, null, values);
        this.closeDataBase();
        Log.d("agregar","insertImc: "+num);

        return num;
    }

    @Override
    public long updateImc(Imc imc) {
        ContentValues values=new ContentValues();

        values.put(DefineTable.HistImc.COLUMN_NAME_ALTURA, imc.getAltura());
        values.put(DefineTable.HistImc.COLUMN_NAME_PESO, imc.getPeso());
        values.put(DefineTable.HistImc.COLUMN_NAME_IMC, imc.getImc());

        this.openDataBase();
        long num = db.update(
                DefineTable.HistImc.TABLE_NAME,
                values,
                DefineTable.HistImc.COLUMN_NAME_ID+"="+imc.getId(),
                null);
        this.closeDataBase();
        Log.d("agregar","updateImc: "+num);

        return num;
    }

    @Override
    public void deleteImcs(int id) {
        this.openDataBase();
        db.delete(
                DefineTable.HistImc.TABLE_NAME,
                DefineTable.HistImc.COLUMN_NAME_ID+"=?",
                new String[] {String.valueOf(id)});
        this.closeDataBase();
    }


    @Override
    public Imc getImc(String id) {
        db = helper.getWritableDatabase();

        String stringId = String.valueOf(id);

        Cursor cursor = db.query(
                DefineTable.HistImc.TABLE_NAME,
                DefineTable.HistImc.REGISTRO,
                DefineTable.HistImc.COLUMN_NAME_ID + " = ?",
                new String[]{stringId},
                null, null, null);

        if (cursor.moveToFirst()) {
            Imc imc = readImc(cursor);
            cursor.close();
            return imc;
        } else {
            cursor.close();
            return null; // No se encontró en la base de datos
        }
    }

    @Override
    public ArrayList<Imc> allImcs() {
        db=helper.getWritableDatabase();

        Cursor cursor=db.query(
                DefineTable.HistImc.TABLE_NAME,
                DefineTable.HistImc.REGISTRO,
                null, null, null, null, null);
        ArrayList<Imc> imcs = new ArrayList<Imc>();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            Imc imc=readImc(cursor);
            imcs.add(imc);
            cursor.moveToNext();
        }

        cursor.close();
        return imcs;
    }

    @Override
    public Imc readImc(Cursor cursor) {
        Imc imc=new Imc();

        imc.setId(cursor.getInt(0));
        imc.setAltura(cursor.getFloat(1));
        imc.setPeso(cursor.getFloat(2));
        imc.setImc(cursor.getFloat(3));

        return imc;
    }

    public void deleteAllImcs() {
        this.openDataBase();
        db.delete(DefineTable.HistImc.TABLE_NAME, null, null);
        this.closeDataBase();
    }


}
