package Modelo;

public class DefineTable {

    public DefineTable(){}

    public static abstract class HistImc{
        public static final String TABLE_NAME="HistorialIMC";
        public static final String COLUMN_NAME_ID="id";
        public static final String COLUMN_NAME_ALTURA="altura";
        public static final String COLUMN_NAME_PESO="peso";
        public static final String COLUMN_NAME_IMC="imc";

        public static String[] REGISTRO = new String[]{
                HistImc.COLUMN_NAME_ID,
                HistImc.COLUMN_NAME_ALTURA,
                HistImc.COLUMN_NAME_PESO,
                HistImc.COLUMN_NAME_IMC
        };
    }

}
