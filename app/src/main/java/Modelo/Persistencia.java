package Modelo;

import com.example.recu_unidad2.Imc;

public interface Persistencia {

    public void openDataBase();
    public void closeDataBase();
    public long insertImc(Imc imc);
    public long updateImc(Imc imc);
    public void deleteImcs(int id);

}
